/// <reference path="../AppGlobals.ts" />

/**
 * @author Jason McTaggart
 */
module App.Auth {

    export var moduleId = App.moduleId + ".Auth";
    export var baseUrl = App.baseUrl + "Auth/";

    export var LS_UserName = "RankIt.Auth.UserName";
    export var LS_UserId = "RankIt.Auth.UserId";
    export var LS_UserToken = "RankIt.Auth.UserToken";
}